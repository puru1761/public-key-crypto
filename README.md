# Public Key CryptoSystems in Golang

This Project showcases 2 public key cryptopsystems developed in Golang. These are namely:

* RSA Cryptosystem
* Rabin Cryptosystem

Each of these Cryptosystems have their sepparate build and run instructions. Each of them have a keygen utility along with encrypt and decrypt functionalities. In order to keep this README short, I will explain each of them together.

## Important Note about Rabin Encrypt/Decrypt

Under my implementation of the Rabin Cryptosystem, in order to give a unique decryption, I pad my plaintext with 128 bits (16 bytes) of '1' prior to encryption. On decryption, I check which root has its 128 LSBs set to 1, and I output the root which does after left shifting it by 128 bits.

## Build and Run Instructions:

Key Generation:
```
$ cd [rabin/rsa]-keygen
$ go build
$ ./[rabin/rsa]-keygen <public-key-filename> <private-key-filename>
```

Encryption:
```
$ cd [rabin/rsa]-encrypt
$ go build
$ ./[rabin/rsa]-encrypt <public-key-filename> <plaintext>
```
The Plaintext MUST be a decimal formatted integer of arbitrary size.

Decryption
```
$ cd [rabin/rsa]-decrypt
$ go build
$ ./[rabin/rsa]-decrypt <private-key-filename> <ciphertext>
```

In addition to this I also implement a ```rabin-factor``` program which takes as an input a Public key and a ciphertext and factorizes the Rabin Modulus by querrying a rabin oracle with random chosen plaintexts. In order to run this program with your own ```rabin-crack``` oracle, please change the command on line 147 of the ```rabin-factor.go``` file to point to your own oracle.

## Author

* Purushottam Kulkarni
* pkulkar6
