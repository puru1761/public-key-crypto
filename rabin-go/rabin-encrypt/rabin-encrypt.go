package main

import (
	"fmt"
	"math/big"
	"os"
	"io/ioutil"
)

func check(e error) {

	if e != nil {
		fmt.Println("\033[91m[X]\033[0m ERROR:", e.Error())
		show_usage()
		os.Exit(-1)
	}
}

func show_usage() {

	fmt.Println("\033[92m[*]\033[0m Rabin Encryption Tool\033[92m[*]\033[0m")
	fmt.Println("This tool can be used to encrypt a plaintext\nusing 1024-bit Rabin Keys and is\n written in the Go Programming Language")
	fmt.Println("")
	fmt.Println("usage:", os.Args[0], "<public-key-filename>", "<plaintext>")
	fmt.Println("\n")
	fmt.Println("\033[94m[A]\033[0m Author: Purushottam Kulkarni")
}


func Exp_Mod_n(x, y, n *big.Int) *big.Int {

	base := x
	exp := y
	mod := n

	if exp.Sign() == 0 {
		return big.NewInt(1)
	}

	x1 := new(big.Int).Set(base)
	x2 := new(big.Int).Mul(base, base)

	for bit:=exp.BitLen()-2;bit>=0;bit-- {

		if exp.Bit(bit) != 0 {

			x1.Mul(x1, x2)
			x2.Mul(x2, x2)
		} else {

			x2.Mul(x2, x1)
			x1.Mul(x1, x1)
		}

		x1.Mod(x1, mod)
		x2.Mod(x2, mod)

	}

	return x1

}

func Mod_Inverse(pub, pN *big.Int) *big.Int {

	val1 := pub
	val2 := pN
	val20 := pN
	x0, x1 := big.NewInt(0), big.NewInt(1)

	for val1.Cmp(big.NewInt(1)) > 0 {

		q := big.NewInt(0).Div(val1, val2)
		tmp := val2
		val2 = big.NewInt(0).Mod(val1, val2)
		val1 = tmp

		tmp = x0
		x0 = big.NewInt(0).Sub(x1, big.NewInt(0).Mul(q, x0))
		x1 = tmp
	}
	if x1.Cmp(big.NewInt(0)) < 0 {
		x1 = big.NewInt(0).Add(x1, val20)
	}

	return x1

}

func rabin_encrypt(plaintext, pub_key_file string) *big.Int {


	pub_data, err := ioutil.ReadFile(pub_key_file)
	check(err)

	pub_data = pub_data[1:len(pub_data)-1]


	N := new(big.Int)
	m := new(big.Int)

	N_string := string(pub_data)

	N.SetString(N_string, 10)
	m.SetString(plaintext, 10)

	pad := new(big.Int)
	pad.SetString("340282366920938463463374607431768211456", 10)
	pad.Sub(pad, big.NewInt(1))


	m.Lsh(m, 128)
	m.Add(m, pad)

	c := Exp_Mod_n(m, big.NewInt(2), N)

	return c
}


func main() {

	if len(os.Args) < 3 {
		show_usage()
		os.Exit(-1)
	}

	pub_key_file := os.Args[1]
	plaintext := os.Args[2]

	ciphertext := rabin_encrypt(plaintext, pub_key_file)


	fmt.Println(ciphertext)
}
