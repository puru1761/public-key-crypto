package main

import (
	"fmt"
	"math/big"
	"crypto/rand"
	"os"
	"io/ioutil"
)

func check(e error) {

	if e != nil {
		fmt.Println("\033[91m[X]\033[0m ERROR:", e.Error())
		show_usage()
		os.Exit(-1)
	}
}

func show_usage() {

	fmt.Println("\033[92m[*]\033[0m Rabin Decryption Tool\033[92m[*]\033[0m")
	fmt.Println("This tool can be used to decrypt a plaintext\nusing 1024-bit Rabin Keys and is\n written in the Go Programming Language")
	fmt.Println("")
	fmt.Println("usage:", os.Args[0], "<private-key-filename>", "<ciphertext>")
	fmt.Println("\n")
	fmt.Println("\033[94m[A]\033[0m Author: Purushottam Kulkarni")
}

func extended_euclidean(a, b *big.Int) (*big.Int, *big.Int, *big.Int) {


	s, old_s := big.NewInt(0), big.NewInt(1)
	t, old_t := big.NewInt(1), big.NewInt(0)
	d, old_d := b, a

	for d.Cmp(big.NewInt(0)) != 0 {

		q := big.NewInt(0).Div(old_d, d)

		temp_d := d
		d = big.NewInt(0).Sub(old_d, big.NewInt(0).Mul(q, d))
		old_d = temp_d

		temp_s := s
		s = big.NewInt(0).Sub(old_s, big.NewInt(0).Mul(q, s))
		old_s = temp_s

		temp_t := t
		t = big.NewInt(0).Sub(old_t, big.NewInt(0).Mul(q, t))
		old_t = temp_t
	}

	return old_d, old_s, old_t


}


func Exp_Mod_n(x, y, n *big.Int) *big.Int {

	base := x
	exp := y
	mod := n

	if exp.Sign() == 0 {
		return big.NewInt(1)
	}

	x1 := new(big.Int).Set(base)
	x2 := new(big.Int).Mul(base, base)

	for bit:=exp.BitLen()-2;bit>=0;bit-- {

		if exp.Bit(bit) != 0 {

			x1.Mul(x1, x2)
			x2.Mul(x2, x2)
		} else {

			x2.Mul(x2, x1)
			x1.Mul(x1, x1)
		}

		x1.Mod(x1, mod)
		x2.Mod(x2, mod)

	}

	return x1

}

func Mod_Inverse(pub, pN *big.Int) (*big.Int, *big.Int) {

	val1 := pub
	val2 := pN
	val20 := pN
	x0, x1 := big.NewInt(0), big.NewInt(1)

	for val1.Cmp(big.NewInt(1)) > 0 {

		q := big.NewInt(0).Div(val1, val2)
		tmp := val2
		val2 = big.NewInt(0).Mod(val1, val2)
		val1 = tmp

		tmp = x0
		x0 = big.NewInt(0).Sub(x1, big.NewInt(0).Mul(q, x0))
		x1 = tmp
	}
	if x1.Cmp(big.NewInt(0)) < 0 {
		x1 = big.NewInt(0).Add(x1, val20)
	}

	return x1, x0

}

func find_roots(c, p, q, N *big.Int) []*big.Int {

	roots := make([]*big.Int, 4)

	p_exp := big.NewInt(0).Div(big.NewInt(0).Add(p, big.NewInt(1)), big.NewInt(4))
	mP := Exp_Mod_n(c, p_exp, p)

	q_exp := big.NewInt(0).Div(big.NewInt(0).Add(q, big.NewInt(1)), big.NewInt(4))
	mQ := Exp_Mod_n(c, q_exp, q)

	_, yP, yQ := extended_euclidean(p, q)

	r := big.NewInt(0).Mod(big.NewInt(0).Add(big.NewInt(0).Mul(yP, big.NewInt(0).Mul(p, mQ)), big.NewInt(0).Mul(yQ, big.NewInt(0).Mul(q, mP))), N)
	roots[0] = r

	_r := big.NewInt(0).Sub(N, r)
	roots[1] = _r

	s := big.NewInt(0).Mod(big.NewInt(0).Sub(big.NewInt(0).Mul(yP, big.NewInt(0).Mul(p, mQ)), big.NewInt(0).Mul(yQ, big.NewInt(0).Mul(q, mP))), N)
	roots[2] = s

	_s := big.NewInt(0).Sub(N, s)
	roots[3] = _s


	return roots
}

func rabin_decrypt(ciphertext, priv_key_file string) *big.Int {

	var N_string string
	var p_string string
	var q_string string


	priv_data, err := ioutil.ReadFile(priv_key_file)
	check(err)

	priv_data = priv_data[1:len(priv_data)-1]

	ind := 0
	flag := false
	for i:=0;i<len(priv_data);i++ {
		if string(priv_data[i]) == "," && ind == 0 {
			N_string = string(priv_data[ind:i])
			ind = i
		} else if string(priv_data[i]) == "," && ind != 0 && flag != true{
			p_string = string(priv_data[ind+1:i])
			ind = i
			flag = true
		} else if flag {
			q_string = string(priv_data[ind+1:len(priv_data)])
			break
		}
	}

	N := new(big.Int)
	p := new(big.Int)
	q := new(big.Int)
	c := new(big.Int)

	N.SetString(N_string, 10)
	p.SetString(p_string, 10)
	q.SetString(q_string, 10)
	c.SetString(ciphertext, 10)

	roots := find_roots(c, p, q, N)

	r, _ := rand.Int(rand.Reader, big.NewInt(4))

	rand_root := roots[r.Int64()]
	return rand_root

}


func main() {

	if len(os.Args) < 3 {
		show_usage()
		os.Exit(-1)
	}

	priv_key_file := os.Args[1]
	plaintext := os.Args[2]

	ciphertext := rabin_decrypt(plaintext, priv_key_file)

	fmt.Printf("%s", ciphertext)

}
