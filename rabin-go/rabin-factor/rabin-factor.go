package main

import (
	"fmt"
	"math/big"
	"crypto/rand"
	"os"
	"io/ioutil"
	"os/exec"
)

func check(e error) {

	if e != nil {
		fmt.Println("\033[91m[X]\033[0m ERROR:", e.Error())
		show_usage()
		os.Exit(-1)
	}
}

func show_usage() {

	fmt.Println("\033[91m[X]\033[0m Rabin Factoring Tool\033[92m[X]\033[0m")
	fmt.Println("This tool can be used to factor a Rabin Modulus\nusing 1024-bit Rabin Key and is\n written in the Go Programming Language")
	fmt.Println("")
	fmt.Println("usage:", os.Args[0], "<public-key-filename>")
	fmt.Println("\n")
	fmt.Println("\033[94m[A]\033[0m Author: Purushottam Kulkarni")
}


func Exp_Mod_n(x, y, n *big.Int) *big.Int {

	base := x
	exp := y
	mod := n

	if exp.Sign() == 0 {
		return big.NewInt(1)
	}

	x1 := new(big.Int).Set(base)
	x2 := new(big.Int).Mul(base, base)

	for bit:=exp.BitLen()-2;bit>=0;bit-- {

		if exp.Bit(bit) != 0 {

			x1.Mul(x1, x2)
			x2.Mul(x2, x2)
		} else {

			x2.Mul(x2, x1)
			x1.Mul(x1, x1)
		}

		x1.Mod(x1, mod)
		x2.Mod(x2, mod)

	}

	return x1

}

func extended_euclid(a, b *big.Int) *big.Int {

	s, old_s := big.NewInt(0), big.NewInt(1)
	t, old_t := big.NewInt(1), big.NewInt(0)
	d, old_d := b, a

	for d.Cmp(big.NewInt(0)) != 0 {

		q := big.NewInt(0).Div(old_d, d)

		temp_d := d
		d = big.NewInt(0).Sub(old_d, big.NewInt(0).Mul(q, d))
		old_d = temp_d

		temp_s := s
		s = big.NewInt(0).Sub(old_s, big.NewInt(0).Mul(q, s))
		old_s = temp_s

		temp_t := t
		t = big.NewInt(0).Sub(old_t, big.NewInt(0).Mul(q, t))
		old_t = temp_t
	}

	return old_d
}

func Mod_Inverse(pub, pN *big.Int) *big.Int {

	val1 := pub
	val2 := pN
	val20 := pN
	x0, x1 := big.NewInt(0), big.NewInt(1)

	for val1.Cmp(big.NewInt(1)) > 0 {

		q := big.NewInt(0).Div(val1, val2)
		tmp := val2
		val2 = big.NewInt(0).Mod(val1, val2)
		val1 = tmp

		tmp = x0
		x0 = big.NewInt(0).Sub(x1, big.NewInt(0).Mul(q, x0))
		x1 = tmp
	}
	if x1.Cmp(big.NewInt(0)) < 0 {
		x1 = big.NewInt(0).Add(x1, val20)
	}

	return x1

}

func rabin_format(pub_key_file string) (*big.Int, *big.Int) {


	pub_data, err := ioutil.ReadFile(pub_key_file)
	check(err)

	pub_data = pub_data[1:len(pub_data)-1]


	N := new(big.Int)
	m, e := rand.Int(rand.Reader, big.NewInt(255))
	check(e)
	original_m := m.String()

	N_string := string(pub_data)

	N.SetString(N_string, 10)

	c := Exp_Mod_n(m, big.NewInt(2), N)

	flag := false
	res1 := new(big.Int)
	res2 := new(big.Int)
	tmp := new(big.Int)

	res1.SetString(original_m, 10)

	for !flag {

		result, err := exec.Command("rabin-crack/rabin-crack", "rabin-crack/pvt_key", c.String()).Output()
		check(err)

		tmp.SetString(string(result), 10)
		inv_check := big.NewInt(0).Add(res1, tmp)

		if string(result) != original_m && inv_check.Cmp(N) != 0 {

			flag = true
			res2.SetString(string(result), 10)
			break
		}

	}

	vuln_sub := big.NewInt(0).Sub(res2, res1)
	if vuln_sub.Sign() < 0 {
		vuln_sub.Mul(vuln_sub, big.NewInt(-1))
	}

	fact1 := extended_euclid(N, vuln_sub)
	fact2 := big.NewInt(0).Div(N, fact1)

	return fact1, fact2
}


func main() {

	if len(os.Args) < 2 {
		show_usage()
		os.Exit(-1)
	}

	pub_key_file := os.Args[1]

	f1, f2 := rabin_format(pub_key_file)
	fmt.Println(f1, f2)

}
