package main

import (
	"bytes"
	"fmt"
	"math"
	"math/big"
	"crypto/rand"
	"os"
	"io/ioutil"
)

func check(e error) {

	if e != nil {
		fmt.Println("\033[91m[X]\033[0m ERROR:", e.Error())
		show_usage()
		os.Exit(-1)
	}
}

func show_usage() {

	fmt.Println("\033[92m[*]\033[0m Rabin Key Generation Tool\033[92m[*]\033[0m")
	fmt.Println("This tool can be used to generate 1024-bit Rabin Keys\nand is written in the Go Programming Language")
	fmt.Println("")
	fmt.Println("usage:", os.Args[0], "<public-key-filename>", "<private-key-filename>")
	fmt.Println("\n")
	fmt.Println("\033[94m[A]\033[0m Author: Purushottam Kulkarni")
}


func Exp_Mod_n(x, y, n *big.Int) *big.Int {

	base := x
	exp := y
	mod := n

	if exp.Sign() == 0 {
		return big.NewInt(1)
	}

	x1 := new(big.Int).Set(base)
	x2 := new(big.Int).Mul(base, base)

	for bit:=exp.BitLen()-2;bit>=0;bit-- {

		if exp.Bit(bit) != 0 {

			x1.Mul(x1, x2)
			x2.Mul(x2, x2)
		} else {

			x2.Mul(x2, x1)
			x1.Mul(x1, x1)
		}

		x1.Mod(x1, mod)
		x2.Mod(x2, mod)

	}

	return x1

}


func is_Prime(n *big.Int) bool {

	var d *big.Int
	var s int

	z := big.NewInt(0).Mod(n, big.NewInt(2))

	if big.NewInt(0).Cmp(z) == 0 {
		return false
	}

	n1 := big.NewInt(0).Sub(n, big.NewInt(1))

	for r:=1;r<64;r++ {

		d = big.NewInt(0).Div(n1, big.NewInt(int64(math.Pow(2, float64(r)))))

		m := big.NewInt(0).Mod(d, big.NewInt(2))
		if big.NewInt(0).Cmp(m) != 0 {

			s = r
			break
		}
	}

	for k:=0;k<40;k++ {

		a, err := rand.Int(rand.Reader, n1)
		check(err)
		x := Exp_Mod_n(a, d, n)

		if x.Cmp(big.NewInt(1)) == 0 || x.Cmp(n1) == 0 {
			continue
		}
		for i:=0;i<s;i++ {

			x = Exp_Mod_n(x, big.NewInt(2), n)
			if x.Cmp(big.NewInt(1)) == 0 {
				return false
			} else if x.Cmp(n1) == 0 {
				break
			}
		}
		if x.Cmp(n1) == 0 {
			continue
		}
		return false
	}


	return true
}


func gen_keys() (*big.Int, *big.Int, *big.Int) {

	var p *big.Int
	var q *big.Int
	var N *big.Int
	var e error

	MAX := new(big.Int)
	MAX.SetString("13407807929942597099574024998205846127479365820592393377723561443721764030073546976801874298166903427690031858186486050853753882811946569946433649006084096", 10)


	for true {
		p, e = rand.Int(rand.Reader, MAX)
		check(e)
		flag1 := is_Prime(p)
		p_check := big.NewInt(0).Mod(p, big.NewInt(4))

		if flag1 && p_check.Cmp(big.NewInt(3))==0{

			for true {
				q, e = rand.Int(rand.Reader, MAX)
				check(e)
				flag2 := is_Prime(q)
				q_check := big.NewInt(0).Mod(q, big.NewInt(4))

				if flag2 && q_check.Cmp(big.NewInt(3)) == 0 {
					break
				}
			}
			break
		}
	}

	N = big.NewInt(0).Mul(p,q)

	return p, q, N
}

func write_pub_key(modulus_string, pub_file string) {

	var buffer bytes.Buffer

	buffer.WriteString("(")
	buffer.WriteString(modulus_string)
	buffer.WriteString(")")

	err := ioutil.WriteFile(pub_file, []byte(buffer.String()), 0666)
	check(err)

	buffer.Reset()
}

func write_priv_key(modulus_string, p_string, q_string, priv_file string) {

	var buffer bytes.Buffer

	buffer.WriteString("(")
	buffer.WriteString(modulus_string)
	buffer.WriteString(",")
	buffer.WriteString(p_string)
	buffer.WriteString(",")
	buffer.WriteString(q_string)
	buffer.WriteString(")")

	e := ioutil.WriteFile(priv_file, []byte(buffer.String()), 0666)
	check(e)

	buffer.Reset()
}

func main() {

	if len(os.Args) < 3 {
		show_usage()
		os.Exit(-1)
	}

	pub_file := os.Args[1]
	priv_file := os.Args[2]

	p, q, N := gen_keys()

	p_string := p.String()
	q_string := q.String()
	modulus_string := N.String()

	write_pub_key(modulus_string, pub_file)
	write_priv_key(modulus_string, p_string, q_string, priv_file)

}
