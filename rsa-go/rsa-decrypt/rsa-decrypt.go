package main

import (
//	"bytes"
	"fmt"
//	"math"
	"math/big"
//	"crypto/rand"
	"os"
	"io/ioutil"
)

func check(e error) {

	if e != nil {
		fmt.Println("\033[91m[X]\033[0m ERROR:", e.Error())
		show_usage()
		os.Exit(-1)
	}
}

func show_usage() {

	fmt.Println("\033[92m[*]\033[0m RSA Decryption Tool\033[92m[*]\033[0m")
	fmt.Println("This tool can be used to decrypt a plaintext\nusing 1024-bit RSA Keys and is\n written in the Go Programming Language")
	fmt.Println("")
	fmt.Println("usage:", os.Args[0], "<private-key-filename>", "<ciphertext>")
	fmt.Println("\n")
	fmt.Println("\033[94m[A]\033[0m Author: Purushottam Kulkarni")
}

func Exp_Mod_n(x, y, n *big.Int) *big.Int {

	base := x
	exp := y
	mod := n

	if exp.Sign() == 0 {
		return big.NewInt(1)
	}

	x1 := new(big.Int).Set(base)
	x2 := new(big.Int).Mul(base, base)

	for bit:=exp.BitLen()-2;bit>=0;bit-- {

		if exp.Bit(bit) != 0 {

			x1.Mul(x1, x2)
			x2.Mul(x2, x2)
		} else {

			x2.Mul(x2, x1)
			x1.Mul(x1, x1)
		}

		x1.Mod(x1, mod)
		x2.Mod(x2, mod)

	}

	return x1

}

func Mod_Inverse(pub, pN *big.Int) *big.Int {

	val1 := pub
	val2 := pN
	val20 := pN
	x0, x1 := big.NewInt(0), big.NewInt(1)

	for val1.Cmp(big.NewInt(1)) > 0 {

		q := big.NewInt(0).Div(val1, val2)
		tmp := val2
		val2 = big.NewInt(0).Mod(val1, val2)
		val1 = tmp

		tmp = x0
		x0 = big.NewInt(0).Sub(x1, big.NewInt(0).Mul(q, x0))
		x1 = tmp
	}
	if x1.Cmp(big.NewInt(0)) < 0 {
		x1 = big.NewInt(0).Add(x1, val20)
	}

	return x1

}

func rsa_decrypt(ciphertext, priv_key_file string) *big.Int {

	var N_string string
	var d_string string

	priv_data, err := ioutil.ReadFile(priv_key_file)
	check(err)

	priv_data = priv_data[1:len(priv_data)-1]

	ind := 0
	for i:=0;i<len(priv_data);i++ {
		if string(priv_data[i]) == "," && ind == 0 {
			N_string = string(priv_data[ind:i])
			ind = i
		} else if string(priv_data[i]) == "," && ind != 0 {
			d_string = string(priv_data[ind+1:i])
			break
		}
	}

	N := new(big.Int)
	d := new(big.Int)
	c := new(big.Int)

	N.SetString(N_string, 10)
	d.SetString(d_string, 10)
	c.SetString(ciphertext, 10)

	m := Exp_Mod_n(c, d, N)

	return m
}


func main() {

	if len(os.Args) < 3 {
		show_usage()
		os.Exit(-1)
	}

	priv_key_file := os.Args[1]
	plaintext := os.Args[2]

	ciphertext := rsa_decrypt(plaintext, priv_key_file)

	fmt.Println(ciphertext)

}
