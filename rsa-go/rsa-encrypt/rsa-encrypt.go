package main

import (
//	"bytes"
	"fmt"
//	"math"
	"math/big"
//	"crypto/rand"
	"os"
	"io/ioutil"
)

func check(e error) {

	if e != nil {
		fmt.Println("\033[91m[X]\033[0m ERROR:", e.Error())
		show_usage()
		os.Exit(-1)
	}
}

func show_usage() {

	fmt.Println("\033[92m[*]\033[0m RSA Encryption Tool\033[92m[*]\033[0m")
	fmt.Println("This tool can be used to encrypt a plaintext\nusing 1024-bit RSA Keys and is\n written in the Go Programming Language")
	fmt.Println("")
	fmt.Println("usage:", os.Args[0], "<public-key-filename>", "<plaintext>")
	fmt.Println("\n")
	fmt.Println("\033[94m[A]\033[0m Author: Purushottam Kulkarni")
}

/**
* Depracated: Super Slow
**/
func Exp_sq(x, y *big.Int) *big.Int {

	mod := big.NewInt(0).Mod(y, big.NewInt(2))

	if big.NewInt(0).Cmp(y) == 0 {
		return big.NewInt(1)
	} else if big.NewInt(1).Cmp(y) == 0 {
		return x
	}

	if mod.Cmp(big.NewInt(0)) == 0 {

		return Exp_sq(big.NewInt(0).Mul(x,x), big.NewInt(0).Div(y, big.NewInt(2)))
	} else {

		return Exp_sq(big.NewInt(0).Mul(x, x), big.NewInt(0).Div(big.NewInt(0).Sub(y,big.NewInt(1)), big.NewInt(2)))
	}
}

func Exp_Mod_n(x, y, n *big.Int) *big.Int {

	base := x
	exp := y
	mod := n

	if exp.Sign() == 0 {
		return big.NewInt(1)
	}

	x1 := new(big.Int).Set(base)
	x2 := new(big.Int).Mul(base, base)

	for bit:=exp.BitLen()-2;bit>=0;bit-- {

		if exp.Bit(bit) != 0 {

			x1.Mul(x1, x2)
			x2.Mul(x2, x2)
		} else {

			x2.Mul(x2, x1)
			x1.Mul(x1, x1)
		}

		x1.Mod(x1, mod)
		x2.Mod(x2, mod)

	}

	return x1

}

func Mod_Inverse(pub, pN *big.Int) *big.Int {

	val1 := pub
	val2 := pN
	val20 := pN
	x0, x1 := big.NewInt(0), big.NewInt(1)

	for val1.Cmp(big.NewInt(1)) > 0 {

		q := big.NewInt(0).Div(val1, val2)
		tmp := val2
		val2 = big.NewInt(0).Mod(val1, val2)
		val1 = tmp

		tmp = x0
		x0 = big.NewInt(0).Sub(x1, big.NewInt(0).Mul(q, x0))
		x1 = tmp
	}
	if x1.Cmp(big.NewInt(0)) < 0 {
		x1 = big.NewInt(0).Add(x1, val20)
	}

	return x1

}

func rsa_encrypt(plaintext, pub_key_file string) *big.Int {

	var N_string string
	var e_string string

	pub_data, err := ioutil.ReadFile(pub_key_file)
	check(err)

	pub_data = pub_data[1:len(pub_data)-1]

	for i:=0;i<len(pub_data);i++ {
		if string(pub_data[i]) == "," {
			N_string = string(pub_data[0:i])
			e_string = string(pub_data[i+1:len(pub_data)])
			break
		}
	}

	N := new(big.Int)
	e := new(big.Int)
	m := new(big.Int)

	N.SetString(N_string, 10)
	e.SetString(e_string, 10)
	m.SetString(plaintext, 10)

	c := Exp_Mod_n(m, e, N)

	return c
}


func main() {

	if len(os.Args) < 3 {
		show_usage()
		os.Exit(-1)
	}

	pub_key_file := os.Args[1]
	plaintext := os.Args[2]

	ciphertext := rsa_encrypt(plaintext, pub_key_file)

	/* Test Code
	d := new(big.Int)
	N := new(big.Int)
	d.SetString("3939281082204042897340339488635256557", 10)
	N.SetString("52140627838940027589280089417681141091", 10)

	m := Exp_Mod_n(ciphertext, d, N)
	fmt.Println(m)
*/
	fmt.Println(ciphertext)

}
