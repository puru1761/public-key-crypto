package main

import (
	"bytes"
	"fmt"
	"math"
	"math/big"
	"crypto/rand"
	"os"
	"io/ioutil"
)

func check(e error) {

	if e != nil {
		fmt.Println("\033[91m[X]\033[0m ERROR:", e.Error())
		show_usage()
		os.Exit(-1)
	}
}

func show_usage() {

	fmt.Println("\033[92m[*]\033[0m RSA Key Generation Tool\033[92m[*]\033[0m")
	fmt.Println("This tool can be used to generate 1024-bit RSA Keys\nand is written in the Go Programming Language")
	fmt.Println("")
	fmt.Println("usage:", os.Args[0], "<public-key-filename>", "<private-key-filename>")
	fmt.Println("\n")
	fmt.Println("\033[94m[A]\033[0m Author: Purushottam Kulkarni")
}

/**
* Depracated: Super Slow
**/
func Exp_sq(x, y *big.Int) *big.Int {

	mod := big.NewInt(0).Mod(y, big.NewInt(2))

	if big.NewInt(0).Cmp(y) == 0 {
		return big.NewInt(1)
	} else if big.NewInt(1).Cmp(y) == 0 {
		return x
	}

	if mod.Cmp(big.NewInt(0)) == 0 {

		return Exp_sq(big.NewInt(0).Mul(x,x), big.NewInt(0).Div(y, big.NewInt(2)))
	} else {

		return Exp_sq(big.NewInt(0).Mul(x, x), big.NewInt(0).Div(big.NewInt(0).Sub(y,big.NewInt(1)), big.NewInt(2)))
	}
}

func Exp_Mod_n(x, y, n *big.Int) *big.Int {

	base := x
	exp := y
	mod := n

	if exp.Sign() == 0 {
		return big.NewInt(1)
	}

	x1 := new(big.Int).Set(base)
	x2 := new(big.Int).Mul(base, base)

	for bit:=exp.BitLen()-2;bit>=0;bit-- {

		if exp.Bit(bit) != 0 {

			x1.Mul(x1, x2)
			x2.Mul(x2, x2)
		} else {

			x2.Mul(x2, x1)
			x1.Mul(x1, x1)
		}

		x1.Mod(x1, mod)
		x2.Mod(x2, mod)

	}

	return x1

}

func Mod_Inverse(pub, pN *big.Int) *big.Int {

	val1 := pub
	val2 := pN
	val20 := pN
	x0, x1 := big.NewInt(0), big.NewInt(1)

	for val1.Cmp(big.NewInt(1)) > 0 {

		q := big.NewInt(0).Div(val1, val2)
		tmp := val2
		val2 = big.NewInt(0).Mod(val1, val2)
		val1 = tmp

		tmp = x0
		x0 = big.NewInt(0).Sub(x1, big.NewInt(0).Mul(q, x0))
		x1 = tmp
	}
	if x1.Cmp(big.NewInt(0)) < 0 {
		x1 = big.NewInt(0).Add(x1, val20)
	}

	return x1

}

func Extended_Euclidean(pub, pN *big.Int) *big.Int {

	val1 := pub
	val2 := pN

	for val1.Cmp(big.NewInt(0)) != 0 && val2.Cmp(big.NewInt(0)) != 0 {

		if val1.Cmp(val2) == 1 {
			val1 = big.NewInt(0).Mod(val1, val2)
		} else if val2.Cmp(val1) == 1 {
			val2 = big.NewInt(0).Mod(val2, val1)
		}
	}

	if val1.Cmp(val2) == 1 {
		return val1
	} else if val2.Cmp(val1) == 1{
		return val2
	}

	return big.NewInt(0)
}

func coPrime(pub_key, pN *big.Int) bool{

	if big.NewInt(1).Cmp(Extended_Euclidean(pub_key, pN)) == 0 {
		return true
	}
	return false
}

func is_Prime(n *big.Int) bool {

	var d *big.Int
	var s int

	z := big.NewInt(0).Mod(n, big.NewInt(2))

	if big.NewInt(0).Cmp(z) == 0 {
		return false
	}

	n1 := big.NewInt(0).Sub(n, big.NewInt(1))

	for r:=1;r<64;r++ {

		d = big.NewInt(0).Div(n1, big.NewInt(int64(math.Pow(2, float64(r)))))

		m := big.NewInt(0).Mod(d, big.NewInt(2))
		if big.NewInt(0).Cmp(m) != 0 {

			s = r
			break
		}
	}

	for k:=0;k<40;k++ {

		a, err := rand.Int(rand.Reader, n1)
		check(err)
		x := Exp_Mod_n(a, d, n)

		if x.Cmp(big.NewInt(1)) == 0 || x.Cmp(n1) == 0 {
			continue
		}
		for i:=0;i<s;i++ {

			x = Exp_Mod_n(x, big.NewInt(2), n)
			if x.Cmp(big.NewInt(1)) == 0 {
				return false
			} else if x.Cmp(n1) == 0 {
				break
			}
		}
		if x.Cmp(n1) == 0 {
			continue
		}
		return false
	}


	return true
}


func gen_keys() (*big.Int, *big.Int, *big.Int, *big.Int, *big.Int) {

	var p *big.Int
	var q *big.Int
	var N *big.Int
	var e error

	MAX := new(big.Int)
	MAX.SetString("13407807929942597099574024998205846127479365820592393377723561443721764030073546976801874298166903427690031858186486050853753882811946569946433649006084096", 10)

	for true {
		p, e = rand.Int(rand.Reader, MAX)
		check(e)
		flag1 := is_Prime(p)

		if flag1 {

			for true {
				q, e = rand.Int(rand.Reader, MAX)
				check(e)
				flag2 := is_Prime(q)

				if flag2 {

					break
				}
			}
			break
		}
	}

	N = big.NewInt(0).Mul(p,q)
	pN := big.NewInt(0).Mul(big.NewInt(0).Sub(p, big.NewInt(1)), big.NewInt(0).Sub(q, big.NewInt(1)))

	for true {

		pub_key, err := rand.Int(rand.Reader, pN)
		check(err)

		if coPrime(pub_key, pN) {

			priv_key := Mod_Inverse(pub_key, pN)

			return pub_key, priv_key, p, q, N
		}
	}

	return nil, nil, nil, nil, nil
}

func write_pub_key(modulus_string, pub_string, pub_file string) {

	var buffer bytes.Buffer

	buffer.WriteString("(")
	buffer.WriteString(modulus_string)
	buffer.WriteString(",")
	buffer.WriteString(pub_string)
	buffer.WriteString(")")

	err := ioutil.WriteFile(pub_file, []byte(buffer.String()), 0666)
	check(err)

	buffer.Reset()
}

func write_priv_key(modulus_string, priv_string, p_string, q_string, priv_file string) {

	var buffer bytes.Buffer

	buffer.WriteString("(")
	buffer.WriteString(modulus_string)
	buffer.WriteString(",")
	buffer.WriteString(priv_string)
	buffer.WriteString(",")
	buffer.WriteString(p_string)
	buffer.WriteString(",")
	buffer.WriteString(q_string)
	buffer.WriteString(")")

	e := ioutil.WriteFile(priv_file, []byte(buffer.String()), 0666)
	check(e)

	buffer.Reset()
}

func main() {

	if len(os.Args) < 3 {
		show_usage()
		os.Exit(-1)
	}

	pub_file := os.Args[1]
	priv_file := os.Args[2]

	e, d, p, q, N := gen_keys()

	pub_string := e.String()
	priv_string := d.String()
	p_string := p.String()
	q_string := q.String()
	modulus_string := N.String()

	write_pub_key(modulus_string, pub_string, pub_file)
	write_priv_key(modulus_string, priv_string, p_string, q_string, priv_file)

}
